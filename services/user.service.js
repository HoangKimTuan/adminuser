var config = require('../config.json');
var _ = require('lodash');
var jwt = require('jsonwebtoken');
var bcrypt = require('bcryptjs');
var Q = require('q');
var mongo = require('mongoskin');
var db = mongo.db(config.connectionString, { native_parser: true });
var dbUser = mongo.db(config.userConnectionString, { native_parser: true });
var roleService = require("../services/systems/role.service");
var collectionConst = require('../utils/system.const').COLLECTION;

db.bind(collectionConst.USERS);
db.bind("level_current");
dbUser.bind("users");
var service = {};

service.authenticate = authenticate;
service.getById = getById;
service.create = create;
service.update = update;
service.delete = _delete;
service.getPage = getPage;
service.updateLevel = updateLevel;
service.getAllUserPending = getAllUserPending;
service.updateCompany = updateCompany;
service.demo = demo;

service.getAll = getAll;
service.getDetailSM = getDetailSM;
service.updateBlock = updateBlock;

module.exports = service;

function authenticate(username, password) {
    var deferred = Q.defer();
    db[collectionConst.USERS].findOne({ username: username }, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user && bcrypt.compareSync(password, user.hash)) {
            // authentication successful
            let getPerm = undefined;
            if (user.userId && user._id.toString() == user.userId.toString()) {
                getPerm = roleService.getFullPerms();
            } else {
                getPerm = roleService.getPerms(user._id);
            }
            
            getPerm.then(function (perms) {
                    let sub = {
                        _id: user._id,
                        perms: perms
                    }
                    deferred.resolve(jwt.sign({ sub: sub }, config.secret));
                })
                .catch(function (err) {
                    deferred.reject("Error when we try to getting your permission");
                })
            
        } else {
            // authentication failed
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getPage(page, filterData) {
    var deferred = Q.defer();
    
    let filter = {};
    if (filterData && filterData.field && filterData.value)
        filter[filterData.field] = new RegExp(filterData.value, "uig");

    db[collectionConst.USERS].find(filter).skip(page * config.numRowPerPage).limit(config.numRowPerPage).toArray(function (err, users) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (users) {
            db[collectionConst.USERS].count(filter, function (err, num) {
                if (err) deferred.reject(err.name + ': ' + err.message);
                
                deferred.resolve({
                    rows: users.map(function (user) {
                        return _.omit(user, 'hash')
                    }),
                    count: num,
                    numRowPerPage: config.numRowPerPage
                });
            })
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function getById(_id) {
    var deferred = Q.defer();

    dbUser.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user) {
            // return user (without hashed password)
            deferred.resolve(_.omit(user, 'hash'));
        } else {
            // user not found
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function create(userParam) {
    var deferred = Q.defer();

    // validation
    db[collectionConst.USERS].findOne(
        { username: userParam.username },
        function (err, user) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (user) {
                // username already exists
                deferred.reject('Username "' + userParam.username + '" is already taken');
            } else {
                createUser();
            }
        });

    function createUser() {
        // set user object to userParam without the cleartext password
        var user = _.omit(userParam, 'password');

        // add hashed password to user object
        user.hash = bcrypt.hashSync(userParam.password, 10);
        user.cre_ts = new Date();

        db[collectionConst.USERS].insert(
            user,
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function update(_id, userParam) {
    var deferred = Q.defer();

    // validation
    db[collectionConst.USERS].findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (user.username !== userParam.username) {
            // username has changed so check if the new username is already taken
            db[collectionConst.USERS].findOne(
                { username: userParam.username },
                function (err, user) {
                    if (err) deferred.reject(err.name + ': ' + err.message);

                    if (user) {
                        // username already exists
                        deferred.reject('Username "' + req.body.username + '" is already taken')
                    } else {
                        updateUser();
                    }
                });
        } else {
            updateUser();
        }
    });

    function updateUser() {
        // fields to update
        var set = {
            mod_ts: new Date(),
            firstName: userParam.firstName,
            lastName: userParam.lastName,
            // username: userParam.username,
        };

        // update password if it was entered
        if (userParam.password) {
            set.hash = bcrypt.hashSync(userParam.password, 10);
        }

        db[collectionConst.USERS].update(
            { _id: mongo.helper.toObjectID(_id) },
            { $set: set },
            function (err, doc) {
                if (err) deferred.reject(err.name + ': ' + err.message);

                deferred.resolve();
            });
    }

    return deferred.promise;
}

function _delete(_id) {
    var deferred = Q.defer();

    db[collectionConst.USERS].remove(
        { _id: mongo.helper.toObjectID(_id) },
        function (err) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve();
        });

    return deferred.promise;
}

function updateLevel(uid, status) {
    var deferred = Q.defer();

    db.level_current.findOne({
            uid: uid
        },
        function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            if (doc) {
                current = doc.current;
                if (status == 1) {
                    current = doc.current + 1;
                }
                var set = {
                    status: status,
                    current: current
                }
                db.level_current.update({
                        _id: mongo.helper.toObjectID(doc._id)
                    }, {
                        $set: set
                    },
                    function (err, doc) {
                        if (err) deferred.reject();

                        deferred.resolve(set);
                    });
            } else {
                deferred.resolve();
            }
        });

    return deferred.promise;
}

function getAllUserPending(level) {
    var deferred = Q.defer();
    var find = {
        status: 2,
        current: parseInt(level, 10)
    };
    if (level < 2) {
        find.companyId = {$exists: false};
    }

    db.level_current.find(find, {uid: 1, _id: 0}).toArray(function (err, doc) {
            if (err) deferred.reject(err.name + ': ' + err.message);

            deferred.resolve(doc);
        });

    return deferred.promise;
}

function updateCompany(uid, companyId) {
    var deferred = Q.defer();

    db.level_current.findOne({
        uid: uid
    },
    function (err, doc) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (doc) {
            var set = {
                companyId: mongo.helper.toObjectID(companyId)
            }
            db.level_current.update({
                    _id: mongo.helper.toObjectID(doc._id)
                }, {
                    $set: set
                },
                function (err, doc) {
                    if (err) deferred.reject();

                    deferred.resolve(set);
                });
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function demo(level) {
    var deferred = Q.defer();

    let aggregateData = [
        {
            "$match": {status: 1, companyId: {$exists: true}}
        }
    ];
    // aggregateData.push({ "$limit": 1 });
    // aggregateData.push({ "$skip" : 0 });

    aggregateData.push({
        $group : {
            _id : "$companyId", 
            count : {$sum : 1},
            uid: {"$push": "$uid"}
        }
    })

    aggregateData.push({
        "$lookup": {
            "from": "ep_companies",
            "localField": "_id",
            "foreignField": "_id",
            "as": "data"
        }
    });

    aggregateData.push({
        "$match": {
            "data.master": {
                $exists: true
            }
        }
    });
    aggregateData.push({"$project": {_id: 1, uid: 1}});

    db.level_current.aggregate(aggregateData, function(err, arr) {
        if (err) {
            deferred.reject(err);
        } else {
            deferred.resolve(arr);
        }
    });

    return deferred.promise;
}


function getAll(page, limit, name) {
    var deferred = Q.defer();

    dbUser.users.count({}, function (err, count) {
        if (err) {
            deferred.reject(err);
        } else {
            var aggregate = [
                { $match : { fullname: { $regex: new RegExp(name) }}},
                { $skip : page * limit },
                { $limit : parseInt(limit, 10) }
            ];
        
            dbUser.users.aggregate(aggregate, function (err, users) {
                if (err) deferred.reject(err.name + ': ' + err.message);
        
                if (users) {
                    deferred.resolve({
                        count: count,
                        numRowPerPage: parseInt(limit, 10),
                        data: users.map(function (user) {
                            return _.omit(user, 'hash')
                        })
                    });
                } else {
                    // user not found
                    deferred.resolve();
                }
            });
        }
    })
    
    return deferred.promise;
}


function getDetailSM(uid) {
    var deferred = Q.defer();

    db.level_current.findOne({
        uid: uid
    },
    function (err, doc) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (doc) {
            deferred.resolve({
                edit: doc.edit,
                chat: doc.chat,
                master: doc.master
            });
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}

function updateBlock(_id, block) {
    var deferred = Q.defer();

    dbUser.users.findById(_id, function (err, user) {
        if (err) deferred.reject(err.name + ': ' + err.message);

        if (block != user.block) {
            // username has changed so check if the new username is already taken
            dbUser.users.update(
                { _id: mongo.helper.toObjectID(_id) },
                { $set: {block: block} },
                function (err, doc) {
                    if (err) deferred.reject(err.name + ': ' + err.message);
    
                    deferred.resolve(doc);
                }
            );
        } else {
            deferred.resolve();
        }
    });

    return deferred.promise;
}