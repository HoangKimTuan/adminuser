module.exports = {
    gender: [{
        "value": "male",
        "en": "Male",
        "vi": "Nam"
    }, {
        "value": "female",
        "en": "Female",
        "vi": "Nữ"
    }, {
        "value": "other",
        "en": "Other",
        "vi": "Khác"
    }], 
    countData: [{
        "value": "totalSearch",
        "en": "Total search",
        "vi": "Số lượt tìm kiếm"
    }, {
        "value": "totalVisitor",
        "en": "Total visitor",
        "vi": "Số lượt truy cập"
    }, {
        "value": "totalCountry",
        "en": "Total country",
        "vi": "Tổng số quốc gia"
    }, {
        "value": "totalSatisfaction",
        "en": "Total satisfaction",
        "vi": "Tổng số hài lòng"
    }],
    status: [{
        "value": "open",
        "en": "Open",
        "vi": "Đang mở"
    }, {
        "value": "close",
        "en": "Close",
        "vi": "Đã đóng"
    }, {
        "value": "delay",
        "en": "Delay",
        "vi": "Delay"
    }]
}