﻿var config = require('../../config.json');
var express = require('express');
var router = express.Router();
var userService = require('../../services/user.service');
// routes
router.post('/authenticate', authenticateUser);
router.post('/register', registerUser);
router.get('/current', getCurrentUser);
router.get('/approve', approve);
router.get('/getAllUserPending', getAllUserPending);
router.get('/updateCompany', updateCompany);
router.get('/demo', demo);
router.get('/:_id', getById);
router.post('/:_id', updateUser);
router.delete('/:_id', deleteUser);
router.post('/pages/:page', getPage);

router.get('/sm/:uid', getDetailSM);
router.post('/updateBlock/:uid', updateBlock);
router.post('/', getAll);

module.exports = router;

function getById(req, res) {
    if (req.body && req.params._id) {
        userService.getById(req.params._id)
            .then(function (user) {
                if (user) {
                    res.send(user);
                } else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    } else {
        res.status(400).send("Not found!");
    }
}

function getPage(req, res) {
    try {
        let page = parseInt(req.params.page);
        userService.getPage(page, req.body)
            .then(function (users) {
                if (users) {
                    res.send(users);
                } else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    } catch (err) {
        res.status(400).send(err);
    }
}

function authenticateUser(req, res) {
    userService.authenticate(req.body.username, req.body.password)
        .then(function (token) {
            if (token) {
                // authentication successful
                res.send({ token: token });
            } else {
                // authentication failed
                res.status(401).send('Username or password is incorrect');
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function registerUser(req, res) {
    userService.create(req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function getCurrentUser(req, res) {
    userService.getById(req.user.sub._id)
        .then(function (user) {
            if (user) {
                res.send(user);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function updateUser(req, res) {
    var userId = req.user.sub._id;
    if (req.params._id !== userId) {
        // can only update own account
        return res.status(401).send('You can only update your own account');
    }

    userService.update(userId, req.body)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function deleteUser(req, res) {
    var userId = req.user.sub._id;
    if (req.params._id !== userId) {
        // can only delete own account
        return res.status(401).send('You can only delete your own account');
    }

    userService.delete(userId)
        .then(function () {
            res.sendStatus(200);
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
}

function approve(req, res) {
    var userId = req.query.uid;
    var status = 1;
    if (req.query.status != "true") {
        status = 3;
    }
    userService.updateLevel(userId, status)
        .then(function (data) {
            if (data) {
                res.send({
                    statusCode: 200,
                    data: data
                });
            } else {
                res.send({
                    statusCode: 403,
                    message: "User not level"
                });
            }
        })
        .catch(function (err) {
            res.send({
                statusCode: 500,
                err: err
            });
        })
}

function getAllUserPending(req, res) {
    userService.getAllUserPending(req.query.level)
        .then(function (data) {
            if (data) {
                res.send({
                    statusCode: 200,
                    data: data
                });
            } else {
                res.send({
                    statusCode: 403,
                    message: "Empty"
                });
            }
        })
        .catch(function (err) {
            res.send({
                statusCode: 500,
                err: err
            });
        })
}

function updateCompany(req, res) {
    userService.updateCompany(req.query.uid, req.query.companyId)
        .then(function (data) {
            if (data) {
                res.send({
                    statusCode: 200,
                    data: data
                });
            } else {
                res.send({
                    statusCode: 403,
                    message: "Empty"
                });
            }
        })
        .catch(function (err) {
            res.send({
                statusCode: 500,
                err: err
            });
        })
}

function demo(req, res) {
    userService.demo(req.query.level)
        .then(function (data) {
            if (data) {
                res.send({
                    statusCode: 200,
                    data: data
                });
            } else {
                res.send({
                    statusCode: 403,
                    message: "Empty"
                });
            }
        })
        .catch(function (err) {
            res.send({
                statusCode: 500,
                err: err
            });
        })
}

function getAll(req, res) {
    if (req.user.sub.perms.sso_users.view) {
        userService.getAll(req.query.page, req.query.limit, req.query.name)
        .then(function (users) {
            if (users) {
                res.send(users);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
    } else {
        res.status(401).send("No permission");
    }
}

function getDetailSM(req, res) {
    if (req.user.sub.perms.sso_users.view) {
        userService.getDetailSM(req.params.uid)
            .then(function (users) {
                if (users) {
                    res.send(users);
                } else {
                    res.sendStatus(404);
                }
            })
            .catch(function (err) {
                res.status(400).send(err);
            });
    } else {
        res.status(401).send("No permission");
    }
}

function updateBlock(req, res) {
    if (req.user.sub.perms.sso_users.edit) {
        userService.updateBlock(req.params.uid, req.body.block)
        .then(function (users) {
            if (users) {
                res.send(users);
            } else {
                res.sendStatus(404);
            }
        })
        .catch(function (err) {
            res.status(400).send(err);
        });
    } else {
        res.status(401).send("No permission");
    }
}