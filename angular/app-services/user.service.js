(function () {
    'use strict';

    angular
        .module('app')
        .factory('UserService', Service);

    function Service($http, $q) {
        var service = {};

        service.GetCurrent = GetCurrent;
        service.GetById = GetById;
        service.GetByUsername = GetByUsername;
        service.Create = Create;
        service.Update = Update;
        service.Delete = Delete;
        service.getPage = getPage;
        service.approve = approve;
        service.getAllUserPending = getAllUserPending;
        service.updateCompany = updateCompany;

        service.GetAll = GetAll;
        service.GetDetailSM = GetDetailSM;
        service.UpdateBlock = UpdateBlock;

        return service;

        function getPage (page, filter) {
            return $http.post('/api/users/pages/' + page, filter);
        }

        function GetCurrent() {
            return $http.get('/api/users/current');
        }

        function GetById(_id) {
            return $http.get('/api/users/' + _id);
        }

        function GetByUsername(username) {
            return $http.get('/api/users/' + username);
        }

        function Create(user) {
            return $http.post('/api/users/register', user);
        }

        function Update(user) {
            return $http.post('/api/users/' + user._id, user);
        }

        function Delete(_id) {
            return $http.delete('/api/users/' + _id);
        }

        function approve(uid, status) {
            return $http.get('/api/users/approve?uid=' + uid + '&status=' + status);
        }

        function getAllUserPending(level) {
            return $http.get('/api/users/getAllUserPending?level=' + level);
        }

        function updateCompany(uid, companyId) {
            return $http.get('/api/users/updateCompany?uid=' + uid + "&companyId=" + companyId);
        }

        function GetAll(page, limit, name) {
            return $http.post('/api/users?page=' + page + "&limit=" + limit + "&name=" + name);
        }

        function GetDetailSM(uid) {
            return $http.get('/api/users/sm/' + uid);
        }

        function UpdateBlock(uid, block) {
            return $http.post('/api/users/updateBlock/' + uid, block);
        }
    }

})();