(function() {
    'use strict';

    angular
        .module('app')
        .controller('User.DetailController', function($q, $rootScope, $location, $http, $scope, $state, $stateParams, Toastr, FormService, LocalStorageService, UserService) {
            var vm = this;
            vm.detail = {};
            
            vm.detailUser = (_id) => {
                UserService.GetById(_id)
                    .then(function (res) {
                        vm.detail = res.data;

                        UserService.GetDetailSM(_id)
                            .then(function (resDetail) {
                                if (resDetail.status == 200) {
                                    vm.detail = {...vm.detail, ...resDetail.data};
                                }
                            })
                            .catch(function (err) {
                                console.log(err);
                            })
                    })
                    .catch(function (err) {
                        console.log(err);
                    })
            }

            vm.detailUser($stateParams._id);
        });
})();