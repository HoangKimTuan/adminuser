(function() {
    'use strict';

    angular
        .module('app')
        .controller('User.IndexController', function($q, $rootScope, $location, $http, $scope, $state, $stateParams, Toastr, FormService, LocalStorageService, UserService) {
            var vm = this;
            vm.listUser = [];
            vm.ft = { name: "", parentId: "", sortVal: 1, sortAttr: "cre_ts" };
            vm.detail = {};
            vm.question= {};
            vm.question.active = true;
            
            vm.loadPage = (i) => {
                UserService.GetAll(i, vm.ft.numRecord ? vm.ft.numRecord : 20, vm.ft.name ? vm.ft.name : "")
                    .then(function (res) {
                        vm.listUser = res.data.data;

                        // $location.search({name: vm.ft.name, numRecord: vm.ft.numRecord, page: i, parentId: vm.ft.parentId, sortVal: vm.ft.sortVal, sortAttr: vm.ft.sortAttr});
                        // $location.replace();
                        // LocalStorageService.setObject("users", {name: vm.ft.name, numRecord: vm.ft.numRecord, page: i, parentId: vm.ft.parentId, sortVal: vm.ft.sortVal, sortAttr: vm.ft.sortAttr})

                        vm.numRowPerPage = res.data.numRowPerPage;
                        vm.pageNums = [...Array(Math.ceil(res.data.count / vm.numRowPerPage)).keys()].splice(i - 4 >= 0 ? i - 4 : 0, 9);
                        vm.currentPage = i;
                        vm.countFrom = vm.currentPage * vm.numRowPerPage + 1;
                        vm.numRecord = res.data.count;
                        vm.numPage = Math.ceil(res.data.count / vm.numRowPerPage);
                        angular.element("table").ready(function () {
                            setTimeout(function () {
                                angular.element("table tr").each((i, e) => { $(e).find(".headcol, .headcol2").height($(e).height() - 25); })
                            })
                        })
                    })
                    .catch(function (err) {
                        console.log(err);
                    })
            }

            vm.loadPage(0);

            vm.ChangeBlock = (index, id) => {
                UserService.UpdateBlock(id, {block: vm.listUser[index].block})
                    .then(function (res) {
                        console.log(res.status)
                    })
                    .catch(function (err) {
                        console.log(err);
                    })
            }

            vm.filter = () => {
                vm.loadPage(0);
            }
        });
})();