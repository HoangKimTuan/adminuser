const COLLECTION = Object.freeze({
    ABOUTUSES: "ep_aboutUses",
    ASSOCIATIONS: "ep_associations",
    CODENAICS: "ep_codeNaics",
    CODENSOS: "ep_codeNsos",
    CODESICS: "ep_codeSics",
    CODETAGS: "ep_codeTags",
    CODEVNS: "ep_codeVns",
    COMPANIES: "ep_companies",
    COMPANYBUSINESSINTROS: "ep_companyBusinessIntros",
    COMPANYBUSINESSTYPES: "ep_companyBusinessTypes",
    COMPANYCATEGORIES: "ep_companyCategories",
    COMPANYCERTIFICATES: "ep_companyCertificates",
    COMPANYCODES: "ep_companyCodes",
    COMPANYCONTACTS: "ep_companyContacts",
    COMPANYCUSTOMERS: "ep_companyCustomers",
    COMPANYDEGREES: "ep_companyDegrees",
    COMPANYDEPARTMENTS: "ep_companyDepartments",
    COMPANYFACILITIES: "ep_companyFacilities",
    COMPANYGROUPCATEGORIES: "ep_companyGroupCategories",
    COMPANYLEGALSTRUCTURES: "ep_companyLegalStructures",
    COMPANYNEWS: "ep_companyNews",
    COMPANYMACHINES: "ep_companyMachines",
    COMPANYPOSITIONS: "ep_companyPositions",
    COMPANYPRODUCTCODES: "ep_companyProductCodes",
    COMPANYPRODUCTS: "ep_companyProducts",
    COMPANYPURCHASINGS: "ep_companyPurchasings",
    COMPANYQUALITYCERTIFICATES: "ep_companyQualityCertificates",
    COMPANYROLES: "ep_companyRoles",
    COMPANYSALES: "ep_companySales",
    COMPANYSHAREHOLDERS: "ep_companyShareholders",
    COMPANYSUPPLIERS: "ep_companySuppliers",
    COMPANYTYPES: "ep_companyTypes",
    CUSTOMERREVIEWS: "ep_customerReviews",
    DASHBOARDS: "ep_dashboards",
    DASHBOARDTYPES: "ep_dashboardTypes",
    DEVELOPMENTMODULES: "ep_developmentModules",
    FAMOUSPLACES: "system_famousPlaces",
    FAQS: "ep_faqs",
    INDUSTRIALPARKS: "ep_industrialParks",
    LANGS: "system_langs",
    LOCATIONTYPES: "system_locationTypes",
    LOGS: "system_logs",
    MAPCOUNTRIES: "system_mapCountries",
    MAPDISTRICTS: "system_mapDistricts",
    MAPGROUPS: "system_mapGroups",
    MAPPLACES: "system_mapPlaces",
    MAPPROVINCES: "system_mapProvinces",
    MAPREGIONS: "system_mapRegions",
    MENUS: "ep_menus",
    NAVS: "ep_navs",
    PARTNERNEWS: "ep_partnerNews",
    PARTNERS: "ep_partners",
    PROVINCECITIES: "ep_provinceCities",
    ROLES: "ep_roles",
    SEARCHQUERIES: "system_searchQueries",
    SETTINGS: "ep_settings",
    SITEDOCUMENTS: "ep_siteDocuments",
    SPECIALZONES: "system_specialZones",
    TAGS: "ep_tags",
    TERMS: "ep_terms",
    UNITS: "system_units",
    UNITTYPES: "system_unitTypes",
    USERACCOUNTS: "ep_userAccounts",
    USERS: "ep_users",
    COMPANYEVENTS: "ep_companyEvents",
    COMPANYNEWS: "ep_companyNews",
    COMPANYPROMOTIONS: "ep_companyPromotions",
    COMPANYJOBS : "ep_companyJobs",
    KEYWORDS: "ep_keywords",
    CODERELATIONS: "ep_codeRelations",
    CONTACTS: "ep_contacts",
    SUBSCRIBES: "ep_subscribes",
    COMPANYFACILITIES: "ep_companyFacilities",
    COMPANYMACHINES: "ep_companyMachines"
})


module.exports = { COLLECTION };